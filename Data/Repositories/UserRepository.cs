﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Data.InMemoryData;
using Data.Repositories.Abstract;
using Entities;

namespace Data.Repositories
{
    public class UserRepository:IUserRepository
    {
        public IEnumerable<User> GetUsers()
        {
            return Database.Users;
        }

        public User AddUser(User user)
        {
            if (!Database.Users.Any())
                user.Id = 1;

            user.Id = Database.Users.Max(o => o.Id) + 1;
            Database.Users.Add(user);
            return user;
        }

        public User GetUserById(int id)
        {
            return Database.Users.FirstOrDefault(u => u.Id == id);
        }

        public void UpdateUser(User user)
        {
            var userToUpdate = GetUserById(user.Id);
            userToUpdate.Name = user.Name;
            userToUpdate.Password = user.Password;
            userToUpdate.Role = user.Role;
        }
    }
}
