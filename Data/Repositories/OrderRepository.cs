﻿using System.Collections.Generic;
using System.Linq;
using Data.InMemoryData;
using Data.Repositories.Abstract;
using Entities;

namespace Data.Repositories
{
    public class OrderRepository : IOrderRepository
    {
        public void UpdateOrder(Order order)
        {
            var orderToChange = GetOrderById(order.Id);
            orderToChange.OrderItems = order.OrderItems;
            orderToChange.Status = order.Status;
            orderToChange.UserId = order.UserId;
        }

        public Order GetOrderById(int id)
        {
            return Database.Orders.FirstOrDefault(o => o.Id == id);
        }

        public void AddOrder(Order order)
        {
            if (!Database.Orders.Any())
                order.Id = 1;

            order.Id = Database.Orders.Max(o => o.Id) + 1;
            Database.Orders.Add(order);
        }

        public IEnumerable<Order> GetOrders()
        {
            return Database.Orders;
        }
    }
}
