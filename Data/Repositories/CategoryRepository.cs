﻿using Data.Repositories.Abstract;
using Entities;
using System.Collections.Generic;
using Data.InMemoryData;


namespace Data.Repositories
{
    class CategoryRepository:ICategoryRepository
    {
        public IEnumerable<Category> GetCategories()
        {
            return Database.Categories;
        }
    }
}
