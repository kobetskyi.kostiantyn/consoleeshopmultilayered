﻿using Data.InMemoryData;
using Data.Repositories.Abstract;
using Entities;
using System.Collections.Generic;
using System.Linq;

namespace Data.Repositories
{
    class ProductRepository : IProductRepository
    {
        public void AddProduct(Product product)
        {
            if (!Database.Products.Any())
                product.Id = 1;
            
            product.Id = Database.Products.Max(p => p.Id) + 1;

            Database.Products.Add(product);
        }

        public void UpdateProduct(Product product)
        {
            var productToUpdate = GetProductById(product.Id);
            productToUpdate.Name = product.Name;
            productToUpdate.CategoryId = product.CategoryId;
            productToUpdate.Description = product.Description;
            productToUpdate.Price = product.Price;
        }

        public IEnumerable<Product> GetProducts()
        {
            return Database.Products;
        }

        public Product GetProductById(int id)
        {
            return Database.Products.FirstOrDefault(p => p.Id == id);
        }
    }
}
