﻿using System;
using System.Collections.Generic;
using System.Text;
using Entities;

namespace Data.Repositories.Abstract
{
    public interface ICategoryRepository
    {
        IEnumerable<Category> GetCategories();
    }
}
