﻿using System;
using System.Collections.Generic;
using System.Text;
using Entities;

namespace Data.Repositories.Abstract
{
    public interface IOrderRepository
    {
        void UpdateOrder(Order order);
        Order GetOrderById(int id);
        void AddOrder(Order order);
        IEnumerable<Order> GetOrders();

    }
}
