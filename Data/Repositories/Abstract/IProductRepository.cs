﻿using System;
using System.Collections.Generic;
using System.Text;
using Entities;

namespace Data.Repositories.Abstract
{
    public interface IProductRepository
    {
        void AddProduct(Product product);
        void UpdateProduct(Product product);
        IEnumerable<Product> GetProducts();
        Product GetProductById(int id);

    }
}
