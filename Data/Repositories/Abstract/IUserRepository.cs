﻿using System;
using System.Collections.Generic;
using System.Text;
using Entities;

namespace Data.Repositories.Abstract
{
   public interface IUserRepository
    {
        IEnumerable<User> GetUsers();
        User AddUser(User user);
        public User GetUserById(int id);
        public void UpdateUser(User user);

    }
}
