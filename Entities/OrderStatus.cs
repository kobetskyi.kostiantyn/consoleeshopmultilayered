﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Entities
{
    public enum OrderStatus
    {
        New = 1,
        CanceledByAdministrator = 2,
        PaymentRecieved = 3,
        Sent = 4,
        Recieved = 5,
        Finished = 6,
        CanceledByUser = 7
    }
}
