﻿namespace Entities
{
    public enum Roles
    {
        Guest = 1,
        RegisteredUser = 2,
        Administrator = 3
    }
} 

