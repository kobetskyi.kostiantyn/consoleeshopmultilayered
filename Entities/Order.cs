﻿using System.Collections.Generic;
using Entities.Abstract;

namespace Entities
{
    public class Order : BaseEntity
    {
        public int UserId { get; set; }
        public OrderStatus Status { get; set; }
        public List<CartItem> OrderItems { get; set; }
    }
}
