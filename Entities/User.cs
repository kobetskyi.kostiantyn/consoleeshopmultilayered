﻿using Entities.Abstract;

namespace Entities
{
    public class User:BaseEntity
    {
        public Roles Role { get; set; }
        public string Name { get; set; }
        public string Password { get; set; }

    }
}
