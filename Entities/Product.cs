﻿using Entities.Abstract;

namespace Entities
{
    public class Product : BaseEntity
    {
        public string Name { get; set; }
        public decimal Price { get; set; }
        public int? CategoryId { get; set; }
        public string Description { get; set; }
    }
}
