﻿using Entities.Abstract;

namespace Entities
{
    public class Category:BaseEntity
    {
        public string Name { get; set; }
    }
}
