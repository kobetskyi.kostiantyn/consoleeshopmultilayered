﻿using System.Collections.Generic;
using System.Linq;

namespace Entities
{
    public class Cart
    {
        public List<CartItem> Items { get; set; } = new List<CartItem>();
        public int ItemsCount => Items?.Sum(p => p.Quantity) ?? 0;
    }
}
