﻿using System.Text;
using Services.Abstract;

namespace Services.ViewServices
{
    public class ProductViewService : IViewService
    {
        private readonly Product product;

        public ProductViewService(Product product)
        {
            this.product = product;
        }
        public string ShowViewData()
        {
            if (product is null)
            {
                return "There are no products to show";
            }

            var sb = new StringBuilder("");
            sb.Append($"Товар: {product.Name}\n" +
                      $"Цена:{product.Price:F} USD\n" +
                      $"Описание:{product.Description}");
            return sb.ToString();
        }
    }
}