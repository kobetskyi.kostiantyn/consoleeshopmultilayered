﻿using System.Text;
using Services.Abstract;

namespace Services.ViewServices
{
    public class UserViewService : IViewService
    {
        private readonly User user;

        public UserViewService(User user)
        {
            this.user = user;
        }
        public string ShowViewData()
        {
            if (user is null)
            {
                return "There are no users to show";
            }

            var sb = new StringBuilder("");
            sb.Append($"Name: {user.Name}\n" +
                      $"Role: {user.Role}");
            return sb.ToString();
        }
    }
}