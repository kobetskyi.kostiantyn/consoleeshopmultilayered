﻿using Services.Abstract;

namespace Services.ViewServices
{
    public class PageDataService : IViewService
    {
        private readonly IViewService menuViewService;
        private readonly IViewService pageViewService;

        public PageDataService(IViewService menuViewService, IViewService pageViewService)
        {
            this.menuViewService = menuViewService;
            this.pageViewService = pageViewService;
        }
        public string ShowViewData()
        {
            return menuViewService.ShowViewData() + pageViewService.ShowViewData();
        }
    }
}