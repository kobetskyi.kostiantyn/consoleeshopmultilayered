﻿using Services.Abstract;

namespace Services.ViewServices
{
    public class StringViewService : IViewService
    {
        private readonly string message;

        public StringViewService(string message)
        {
            this.message = message;
        }
        public string ShowViewData()
        {
            return message;
        }
    }
}