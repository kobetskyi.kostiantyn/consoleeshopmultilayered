﻿namespace Services.Abstract
{
    public interface IViewService
    {
        string ShowViewData();
    }
}