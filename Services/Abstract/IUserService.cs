﻿namespace Services.Abstract
{
    public interface IUserService
    {
        User GetUserByName(string name);
    }
}
